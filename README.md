This project contains a test flow where user can enter all the required details for creating a policy and at the end after confirming the terms and conditions, also privacy policy should be able to download his/her policy.

So in this we have created the test script which will cover the entire flow using Java Selenium.

Below project contains below tools
a. Maven - used for building the project and can also help to execute our test cases
b. TestNG - used for generating the unit test and also helps to rerun the failed test cases
c. Selenium WebDriver - used for UI automation
d. Java - used as a programming language to automate the UI using selenium dependencies
e. Fillo - used to read the data from the excel sheet

Flow description:
In this project we are using the page object model concept and the flow is very simple with respect to other tools.
1. We have a test base class which is helping us to initiate the browser based on the user input and launching the application in the same browser.
2. CommonEvent class - Used for all selenium events like waiting for an element to be located and visible, click operation, entering value to a text field etc.
3. Retry Analyser - helps us to retry the failed test cases programatically
4. FileOperations - helps us to read the data from differerent file sources like excel, properties and json
5. ExtentReportManager - used to generate the visual report in extent report

The project contains a folder called "src". This folder has two different sections called main and test. We can get the test case under src/test and all supportive files like chrome driver to initiate the chrome browser, test data sheet, property files are under main.

Note:
1. I've uploaded the coreframework jar which will be the supporting jar to perform all types of events.
2. Execution video uploaded to the same repo for reference.
3. Related to the other test scenarios on the same app, we can refer PolicyTest file in the same repo.
4. related to the OTP based test, we can refer OTPTest file in the same repo.
5. Related to the CI pipeline. We can refer the file called pipeline in the same repo.
6. Related to performing the visual test, we can refer the VisualTest file in the same repo.
