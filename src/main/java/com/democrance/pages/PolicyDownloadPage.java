package com.democrance.pages;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PolicyDownloadPage extends StartupPage
{
	//Page elements
	By downloadPolicyButton = By.xpath("//span[text()='Download Policy schedule']");
	By policyNumber = By.xpath("//h1[@class='subtitle block']");
	
	//Getting the page name
	String pageName = this.getClass().getSimpleName();

	public PolicyDownloadPage(WebDriver driver) {
		super(driver);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
	}
	
	public String getPolicyNumber()
	{
		String actualPolicyNumber = commonEvent.waitTillElementLocated(policyNumber)
												.waitTillElementVisible(policyNumber)
												.getText(policyNumber);
		String[] split = actualPolicyNumber.split(" ");
		return split[1].replace("#", "");
	}
	
	public void clickDownloadPolicy() throws Exception
	{
		commonEvent.waitTillElementLocated(downloadPolicyButton)
					.waitTillElementVisible(downloadPolicyButton)
					.click(downloadPolicyButton, "Download policy button", pageName);
	}
	

}
