package com.democrance.pages;

import java.io.File;

import org.openqa.selenium.WebDriver;

import coreFramework.utils.CommonEvents;



public class StartupPage 
{
	public WebDriver driver;
	public CommonEvents commonEvent;
	String fileSeparator = File.separator;
	public String baseImageFolderPath = System.getProperty("user.dir")+fileSeparator+"BaseImages"+fileSeparator;
	
	public StartupPage(WebDriver driver)
	{
		this.driver=driver;
		commonEvent = new CommonEvents(this.driver);
	}

}
