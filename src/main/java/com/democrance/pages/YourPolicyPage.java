package com.democrance.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class YourPolicyPage extends StartupPage
{
	//Page Elements
	By termsConditionsCheckBox = By.xpath("//a[text()=' Terms & Conditions']/../preceding-sibling::span");
	By privacyPolicyCheckBox = By.xpath("//a[text()=' Privacy Policy']/../preceding-sibling::span");
	By backToFormButton = By.xpath("//button[text()='Back to Form']");
	By confirmAndBuyNowButton = By.xpath("//button[text()='Confirm and Buy Now']");
	
	//Getting the page name
	String pageName = this.getClass().getSimpleName();

	public YourPolicyPage(WebDriver driver) {
		super(driver);
	}
	
	public YourPolicyPage checkTermsAndConditionsBox() throws Exception
	{
		commonEvent.waitTillElementLocated(termsConditionsCheckBox)
					.jsScrollPageTillElementVisible(termsConditionsCheckBox)
					.waitTillElementVisible(termsConditionsCheckBox)
					.click(termsConditionsCheckBox, "Terms and Conditions check box", pageName);
		return new YourPolicyPage(driver);
	}
	
	public YourPolicyPage checkPrivacyPolicyBox() throws Exception
	{
		commonEvent.waitTillElementLocated(privacyPolicyCheckBox)
					.jsScrollPageTillElementVisible(privacyPolicyCheckBox)
					.waitTillElementVisible(privacyPolicyCheckBox)
					.click(privacyPolicyCheckBox, "Privacy Policy Check Box", pageName);
		return new YourPolicyPage(driver);
	}
	
	public BeneficiaryDetailsPage clickBackToForm() throws Exception
	{
		commonEvent.waitTillElementLocated(backToFormButton)
					.waitTillElementVisible(backToFormButton)
					.click(backToFormButton, "Back To Form button", pageName);
		return new BeneficiaryDetailsPage(driver);
	}
	
	public PolicyDownloadPage clickConfirmAndBuyNow() throws Exception
	{
		commonEvent.waitTillElementLocated(confirmAndBuyNowButton)
					.waitTillElementVisible(confirmAndBuyNowButton)
					.click(confirmAndBuyNowButton, "Confirm And Buy Now button", pageName);
		return new PolicyDownloadPage(driver);
	}
	

}
