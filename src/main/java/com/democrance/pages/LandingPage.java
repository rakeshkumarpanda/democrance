package com.democrance.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LandingPage extends StartupPage
{
	//Element details
	By firstName = By.name("first_name");
	By lastName = By.name("last_name");
	By dateOfBirthSelector = By.xpath("//span[text()='Date of Birth']/../following-sibling::div//div[@role='button']");
	By monthSelector = By.xpath("//div[@class='field has-addons']/div[1]/span[@class='select']");
	By yearSelector = By.xpath("//div[@class='field has-addons']/div[2]/span[@class='select']");
	By maleGender = By.xpath("//span[text()='Male']");
	By femaleGender = By.xpath("//span[text()='Female']");
	By nationalitySelector = By.cssSelector("input[data-type='select']");
	By email = By.name("email");
	By isdCodeSelectorArrow = By.cssSelector("i[class='fas fa-angle-down']");
	By mobileNumber = By.cssSelector("input[type='tel']");
	By emiratesIdNumber = By.name("national_id_number");
	By nextButton = By.xpath("//button[text()='Next: Beneficiary Details']");
	
	String dateToBeSelected = "//a[span[text()='Date']]";
	String countryPath = "//div[contains(text(),' Country ')]";
	String countryISD = "//strong[text()='Country']";
	
	//Getting the page name
	String pageName = this.getClass().getSimpleName();

	public LandingPage(WebDriver driver) {
		super(driver);
	}
	
	public LandingPage enterFirstName(String firstNameValue) throws Exception
	{
		commonEvent.waitTillElementLocated(firstName)
					.waitTillElementVisible(firstName)
					.sendKeys(firstName, "First Name Field", pageName, firstNameValue);
		return new LandingPage(driver);
	}
	
	public LandingPage enterlastName(String lastNameValue) throws Exception
	{
		commonEvent.waitTillElementLocated(lastName)
					.waitTillElementVisible(lastName)
					.sendKeys(lastName, "Last Name Field", pageName, lastNameValue);
		return new LandingPage(driver);
	}
	
	public LandingPage enterDob(String date, String month, String year) throws Exception
	{
		commonEvent.waitTillElementLocated(dateOfBirthSelector)
					.waitTillElementVisible(dateOfBirthSelector)
					.click(dateOfBirthSelector, "DOB Selector", pageName);
//		commonEvent.waitTillElementLocated(monthSelector)
//					.waitTillElementVisible(monthSelector)
//					.click(monthSelector, "Month Selector", pageName);
//		commonEvent.waitTillElementLocated(yearSelector)
//					.waitTillElementVisible(yearSelector)
//					.selectByVisibleText(yearSelector, "Year Selector", pageName, year);
		String desiredDatePath = dateToBeSelected.replace("Date", date);
		By desiredDateElement = By.xpath(desiredDatePath);
		commonEvent.waitTillElementLocated(desiredDateElement)
					.waitTillElementVisible(desiredDateElement)
					.click(desiredDateElement, date+" for "+month+" "+year, "Date picker dialog");
		return new LandingPage(driver);
		
	}
	
	public LandingPage selectMaleGender() throws Exception
	{
		commonEvent.waitTillElementLocated(maleGender)
					.waitTillElementVisible(maleGender)
					.click(maleGender, "Male Gender Button", pageName);
		return new LandingPage(driver);
	}
	
	public LandingPage selectFemaleGender() throws Exception
	{
		commonEvent.waitTillElementLocated(femaleGender)
					.waitTillElementVisible(femaleGender)
					.click(femaleGender, "Female Gender Button", pageName);
		return new LandingPage(driver);
	}
	
	public LandingPage selectNationality(String country) throws Exception
	{
		commonEvent.waitTillElementLocated(nationalitySelector)
					.waitTillElementVisible(nationalitySelector)
					.click(nationalitySelector, "Nationality Field", pageName);
		
		String desiredCountry = countryPath.replace(" Country ", " "+country+" ");
		By desiredCountryElement = By.xpath(desiredCountry);
		
		commonEvent.waitTillElementLocated(desiredCountryElement)
					.waitTillElementVisible(desiredCountryElement)
					.jsScrollPageTillElementVisible(desiredCountryElement)
					.actionsClick(desiredCountryElement);
		return new LandingPage(driver);
	}
	
	public LandingPage enterEmailAddress(String emailAddress) throws Exception
	{
		commonEvent.waitTillElementLocated(email)
					.waitTillElementVisible(email)
					.sendKeys(email, "Email Address Field", pageName, emailAddress);
		return new LandingPage(driver);
	}
	
	public LandingPage enterMobileNumber(String mobile) throws Exception
	{
		commonEvent.waitTillElementLocated(mobileNumber)
					.waitTillElementVisible(mobileNumber)
					.sendKeys(mobileNumber, "Mobile Number Field", pageName, mobile);
		return new LandingPage(driver);
	}
	
	public LandingPage enterEmiratesId(String eid) throws Exception
	{
		commonEvent.waitTillElementLocated(emiratesIdNumber)
					.waitTillElementVisible(emiratesIdNumber)
					.sendKeys(emiratesIdNumber, "Emirated Id Number Field", pageName, eid);
		return new LandingPage(driver);
	}
	
	public BeneficiaryDetailsPage clickNextButton() throws Exception
	{
		commonEvent.waitTillElementLocated(nextButton)
					.waitTillElementVisible(nextButton)
					.click(nextButton, "Next: Beneficiary Details", pageName);
		return new BeneficiaryDetailsPage(driver);
	}

}
