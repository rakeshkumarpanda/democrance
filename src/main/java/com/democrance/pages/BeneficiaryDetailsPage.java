package com.democrance.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BeneficiaryDetailsPage extends StartupPage
{
	//Page Elements
	By firstName = By.name("first_name");
	By lastName = By.name("last_name");
	By spouseButton = By.xpath("//span[text()='Spouse']");
	By childButton = By.xpath("//span[text()='Child']");
	By parentButton = By.xpath("//span[text()='Parent']");
	By maleGender = By.xpath("//span[text()='Male']");
	By femaleGender = By.xpath("//span[text()='Female']");
	By isdCodeSelectorArrow = By.cssSelector("i[class='fas fa-angle-down']");
	By mobileNumber = By.cssSelector("input[type='tel']");
	By backToFormButton = By.xpath("//button[text()='Back to Form']");
	By nextButton = By.xpath("//button[text()='Next: Your Policy']");

	//Getting the page name
	String pageName = this.getClass().getSimpleName();
	
	public BeneficiaryDetailsPage(WebDriver driver) {
		super(driver);
	}
	
	public BeneficiaryDetailsPage enterFirstName(String firstNameValue) throws Exception
	{
		Thread.sleep(3000);
		commonEvent.waitTillElementLocated(firstName)
					.waitTillElementVisible(firstName)
					.sendKeys(firstName, "First Name Field", pageName, firstNameValue);
		return new BeneficiaryDetailsPage(driver);
	}
	
	public BeneficiaryDetailsPage enterlastName(String lastNameValue) throws Exception
	{
		commonEvent.waitTillElementLocated(lastName)
					.waitTillElementVisible(lastName)
					.sendKeys(lastName, "Last Name Field", pageName, lastNameValue);
		return new BeneficiaryDetailsPage(driver);
	}
	
	public BeneficiaryDetailsPage selectMaleGender() throws Exception
	{
		commonEvent.waitTillElementLocated(maleGender)
					.waitTillElementVisible(maleGender)
					.click(maleGender, "Male Gender Button", pageName);
		return new BeneficiaryDetailsPage(driver);
	}
	
	public BeneficiaryDetailsPage selectFemaleGender() throws Exception
	{
		commonEvent.waitTillElementLocated(femaleGender)
					.waitTillElementVisible(femaleGender)
					.click(femaleGender, "Female Gender Button", pageName);
		return new BeneficiaryDetailsPage(driver);
	}
	
	public BeneficiaryDetailsPage selectSpouse() throws Exception
	{
		commonEvent.waitTillElementLocated(spouseButton)
					.waitTillElementVisible(spouseButton)
					.click(spouseButton, "Spouse Button", pageName);
		return new BeneficiaryDetailsPage(driver);
	}
	
	public BeneficiaryDetailsPage selectChild() throws Exception
	{
		commonEvent.waitTillElementLocated(childButton)
					.waitTillElementVisible(childButton)
					.click(childButton, "Child Button", pageName);
		return new BeneficiaryDetailsPage(driver);
	}
	
	public BeneficiaryDetailsPage selectParent() throws Exception
	{
		commonEvent.waitTillElementLocated(parentButton)
					.waitTillElementVisible(parentButton)
					.click(parentButton, "Parent Button", pageName);
		return new BeneficiaryDetailsPage(driver);
	}
	
	public BeneficiaryDetailsPage enterMobileNumber(String mobile) throws Exception
	{
		commonEvent.waitTillElementLocated(mobileNumber)
					.waitTillElementVisible(mobileNumber)
					.sendKeys(mobileNumber, "Mobile Number Field", pageName, mobile);
		return new BeneficiaryDetailsPage(driver);
	}
	
	public LandingPage clickBackToFormButtom() throws Exception
	{
		commonEvent.waitTillElementLocated(backToFormButton)
					.waitTillElementVisible(backToFormButton)
					.click(backToFormButton, "Back To Form button", pageName);
		return new LandingPage(driver);
	}
	
	public YourPolicyPage clickNextButton() throws Exception
	{
		commonEvent.waitTillElementLocated(nextButton)
					.waitTillElementVisible(nextButton)
					.click(nextButton, "Next: Your Policy button", pageName);
		return new YourPolicyPage(driver);
	}

}
