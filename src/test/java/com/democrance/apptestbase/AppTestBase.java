package com.democrance.apptestbase;

import java.io.File;
import java.util.Map;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.asserts.SoftAssert;

import coreFramework.testbase.TestBase;
import coreFramework.utils.FileOperation;

public class AppTestBase extends TestBase
{
	public Map<String, String> envData;
	public SoftAssert softAssert;
	public String domain;
	String fileSeparator = File.separator;
	public String baseResourceFilePath = System.getProperty("user.dir")+fileSeparator+"src"+fileSeparator+"main"+fileSeparator+"resources"+fileSeparator;
	public String configFilePath = baseResourceFilePath+"config.properties";
	public String filloQueryFilePath = baseResourceFilePath+"FilloQueries.properties";
	public String excelTestDataFilePath = baseResourceFilePath+"TestData.xlsx";
	public Map<String, String> filloQueries;
	
	@Parameters({"browser"})
	@BeforeClass(alwaysRun = true)
	public void init(String browser) throws Exception
	{
		softAssert = new SoftAssert();
		envData = new FileOperation().readProperty(configFilePath);
		initialize(browser, envData);
		filloQueries = new FileOperation().readProperty(filloQueryFilePath);
	}
	
	@AfterClass(alwaysRun = true)
	public void tearDown() throws InterruptedException
	{
		Thread.sleep(3000);
		browserTearDown();
	}

}
