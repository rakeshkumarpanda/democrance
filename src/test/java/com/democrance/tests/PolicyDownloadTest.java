package com.democrance.tests;

import java.util.Map;

import org.testng.annotations.Test;

import com.democrance.apptestbase.AppTestBase;
import com.democrance.pages.BeneficiaryDetailsPage;
import com.democrance.pages.LandingPage;
import com.democrance.pages.PolicyDownloadPage;
import com.democrance.pages.YourPolicyPage;

import coreFramework.utils.FileOperation;

public class PolicyDownloadTest extends AppTestBase 
{
	@Test
	public void policyDownloadTest() throws Exception
	{
		Map<String, String> policyDownloadData = new FileOperation()
												.readExcel(excelTestDataFilePath,filloQueries.get("policyDownload"));
		
		LandingPage landingPage = new LandingPage(driver);
		BeneficiaryDetailsPage beneficiaryPage = landingPage.enterFirstName(policyDownloadData.get("P_FIRSTNAME"))
															.enterlastName(policyDownloadData.get("P_LASTNAME"))
															.enterDob("1", "January", "2022")
															.selectMaleGender()
															.selectNationality(policyDownloadData.get("NATIONALITY"))
															.enterEmailAddress(policyDownloadData.get("EMAIL"))
															.enterMobileNumber(policyDownloadData.get("MOBILE"))
															.enterEmiratesId(policyDownloadData.get("EID"))
															.clickNextButton();
		YourPolicyPage yourPolicyPage = beneficiaryPage.enterFirstName(policyDownloadData.get("B_FIRSTNAME"))
														.enterlastName(policyDownloadData.get("B_LASTNAME"))
														.selectSpouse()
														.selectFemaleGender()
														.enterMobileNumber(policyDownloadData.get("MOBILE"))
														.clickNextButton();
		PolicyDownloadPage policyDownloadPage = yourPolicyPage.checkTermsAndConditionsBox()
																.checkPrivacyPolicyBox()
																.clickConfirmAndBuyNow();
		String policyNumber = policyDownloadPage.getPolicyNumber();
		System.out.println(policyNumber);
		policyDownloadPage.clickDownloadPolicy();
	}

}
